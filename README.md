# Desafio Airflow

## Objetivo

- Crie uma task que lê os dados da tabela 'Order' do banco de dados disponível em data/Northwhind_small.sqlite. O formato do banco de dados é o Sqlite3. Essa task deve escrever um arquivo chamado "output_orders.csv".
- Crie uma task que lê os dados da tabela "OrderDetail" do mesmo banco de dados e faz um JOIN com o arquivo "output_orders.csv" que você exportou na tarefa anterior. Essa task deve calcular qual a soma da quantidade vendida (Quantity) com destino (ShipCity) para o Rio de Janeiro. Você deve exportar essa contagem em arquivo "count.txt" que contenha somente esse valor em formato texto.
- Crie uma ordenação de execução das Tasks que deve terminar com a task export_final_output.

## Solução

Primeiramente criamos o arquivo desafio-3-dag.py, nele são importadas as bibliotecas necessárias para a criação e execução da DAG. Além disso, criamos e importamos o arquivo read_db.py, que contém as funções que serão utilizadas para a execução das tasks. 

```py
from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from os import path
from airflow.models import Variable

import read_db
```

Em read_db.py importamos as bibliotecas que serão utilizadas posteriormente.

```py
from os import path

import sqlite3
import csv
import pandas as pd
```

Define-se então os argumentos padrão, que serão aplicados a todas as tasks, e inicia-se a DAG, definindo seu nome, intervalo de execução e etc.

```py
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email': ['vinicius@indicium.tech'],
    'email_on_failure': True,
    'email_on_retry': True,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

with DAG(
    'Desafio-3-Lighthouse',
    default_args=default_args,
    description='',
    schedule_interval=timedelta(days=1),
    start_date=datetime(2021, 1, 1),
    catchup=False,
    tags=['example'],
) as dag:
    dag.doc_md = """
        Esse é o desafio de Airflow da Indicium.
    """
```

### Task order_to_csv

Esta é a primeira task que sera executada pela DAG. Ela chama a função order_to_csv, presente em read_db.py, que resolverá o primeiro objetivo do desafio.

```py
order_to_csv = PythonOperator(
    task_id='order_to_csv',
    python_callable=read_db.order_to_csv,
    provide_context=True
)
```

Em read_db.py definimos a função connect_db(), que, recebendo o caminho para o arquivo do banco de dados, cria e retorna uma conexão com o mesmo

```py
def connect_db(db_file):
    """ 
    Create a connection to the SQLite database
    specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except sqlite3.Error as e:
        print(e)

    return conn
```

Criamos então as variáveis para armazenar o diretório raiz do projeto, o caminho para o banco de dados, a conexão estabelecida por connect_db() e o crusor da conexão.

```py
BASE_DIR = path.join(path.dirname(path.abspath(__file__)), "../../")
db_path = path.join(BASE_DIR, "data/Northwind_small.sqlite")
conn = connect_db(db_path)
cur = conn.cursor()
```

Enfim definimos o método order_to_csv(), que é chamado pela task order_to_csv. Com o cursor, realizamos uma query de todas os valores da tabela Order, armazenando todas as linhas retornadas no arquivo output_orders.csv, utilizando csv.writer().

```py
def order_to_csv():
    """
    Query all rows in the Order table and write them into output_orders.csv
    :param:
    :return:
    """ 
    
    cur.execute("SELECT * FROM 'Order'")
    rows = cur.fetchall()

    with open(path.join(BASE_DIR, 'outputs/output_orders.csv'), 'w') as csvfile:
        csv_writer = csv.writer(csvfile)
        for row in rows:
            csv_writer.writerow(row)
```

### Task count_quantity_rio

Esta task chama a função read_db.count_quantity_rio(), que irá calcular a quantidade de produtos enviados para o Rio de Janeiro.

```py
count_quantity_rio = PythonOperator(
    task_id='count_quantity_rio',
    python_callable=read_db.count_quantity_rio,
    provide_context=True
)
```

Definimos então em read_db a função orderdatail_to_df(), que cria um pandas dataframe selecionando todos os valores da tabela OrderDetail. 

```py
def orderdetail_to_df():
    """
    Query all rows in the OrderDetail table into a pandas DataFrame
    :param:
    :return:
    """ 

    cur.execute('SELECT * FROM OrderDetail')
    rows = cur.fetchall()

    df = pd.DataFrame(rows)

    return df
```

Na função count_quantity_rio(), transformamos os valores do arquivo output_orders.csv, criado na task anterior, em um pandas dataframe. Deste dataframe, separamos apenas as linhas que contenham a coluna 10 (equivalente a ShipCity) igual a "Rio de Janeiro". Criamos um dataframe para a tablea OrderDetails e fazemos um merge() com o dataframe order_rio_df, onde comparamos a coluna 1 (equivalente a OrderId) de orderdetail_df com a coluna 0 (equivalente a Id) de order_rio_df. Desta forma, temos um dataframe com todos os valores de OrderDetail dos pedidos feitos para o Rio de Janeiro, e apenas realizamos a soma de todos os valores na coluna 4_x (equivalente a Quantity) do mesmo. O resultado é exportado para o arquivo count.txt.

```py
def count_quantity_rio():
    """
    Query all rows in the OrderDetail table into a pandas DataFrame
    :param:
    :return:
    """ 

    output_orders_path = path.join(BASE_DIR, "outputs/output_orders.csv")
    order_df = pd.read_csv(output_orders_path, header=None)
    order_rio_df = order_df[order_df[10] == "Rio de Janeiro"]
    
    orderdetail_df = orderdetail_to_df()

    merge_df = pd.merge(orderdetail_df, order_rio_df, left_on=1, right_on=0)
    sum_qtt = merge_df['4_x'].sum()

    with open(path.join(BASE_DIR, 'outputs/count.txt'), 'w') as txt:
        txt.write(str(sum_qtt))
```

### Task export_final_output

A última task da DAG chama a função export_final_answer(), que codificará o email salvo na variável my_email do Airflow junto com o valor salvo em count.txt, salvando-os no arquivo final_output.txt.
 
```py
BASE_DIR = path.join(path.dirname(path.abspath(__file__)), "../../")

def export_final_answer():
    import base64

    # Import count
    with open(path.join(BASE_DIR, 'outputs/count.txt')) as f:
        count = f.readlines()[0]

    my_email = Variable.get("my_email")
    message = my_email+count
    message_bytes = message.encode('ascii')
    base64_bytes = base64.b64encode(message_bytes)
    base64_message = base64_bytes.decode('ascii')

    with open("final_output.txt","w") as f:
        f.write(base64_message)
    return None

export_final_output = PythonOperator(
        task_id='export_final_output',
        python_callable=export_final_answer,
        provide_context=True
    )
```

### Ordenação das tasks

Utilizamos então o sinal de menor-que para definirmos a ordem de execução das tasks.

```py
order_to_csv >> count_quantity_rio >> export_final_output
```
from os import path

import sqlite3
import csv
import pandas as pd


def connect_db(db_file):
    """ 
    Create a connection to the SQLite database
    specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except sqlite3.Error as e:
        print(e)

    return conn


BASE_DIR = path.join(path.dirname(path.abspath(__file__)), "../../")
db_path = path.join(BASE_DIR, "data/Northwind_small.sqlite")
conn = connect_db(db_path)
cur = conn.cursor()


def order_to_csv():
    """
    Query all rows in the Order table and write them into output_orders.csv
    :param:
    :return:
    """ 
    
    cur.execute("SELECT * FROM 'Order'")
    rows = cur.fetchall()

    with open(path.join(BASE_DIR, 'outputs/output_orders.csv'), 'w') as csvfile:
        csv_writer = csv.writer(csvfile)
        for row in rows:
            csv_writer.writerow(row)


def orderdetail_to_df():
    """
    Query all rows in the OrderDetail table into a pandas DataFrame
    :param:
    :return:
    """ 

    cur.execute('SELECT * FROM OrderDetail')
    rows = cur.fetchall()

    df = pd.DataFrame(rows)

    return df



def count_quantity_rio():
    """
    Query all rows in the OrderDetail table into a pandas DataFrame
    :param:
    :return:
    """ 

    output_orders_path = path.join(BASE_DIR, "outputs/output_orders.csv")
    order_df = pd.read_csv(output_orders_path, header=None)
    order_rio_df = order_df[order_df[10] == "Rio de Janeiro"]
    
    orderdetail_df = orderdetail_to_df()

    merge_df = pd.merge(orderdetail_df, order_rio_df, left_on=1, right_on=0)
    sum_qtt = merge_df['4_x'].sum()

    with open(path.join(BASE_DIR, 'outputs/count.txt'), 'w') as txt:
        txt.write(str(sum_qtt))

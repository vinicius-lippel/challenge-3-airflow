from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from os import path
from airflow.models import Variable

import read_db

BASE_DIR = path.join(path.dirname(path.abspath(__file__)), "../../")

def export_final_answer():
    import base64

    # Import count
    with open(path.join(BASE_DIR, 'outputs/count.txt')) as f:
        count = f.readlines()[0]

    my_email = Variable.get("my_email")
    message = my_email+count
    message_bytes = message.encode('ascii')
    base64_bytes = base64.b64encode(message_bytes)
    base64_message = base64_bytes.decode('ascii')

    with open("final_output.txt","w") as f:
        f.write(base64_message)
    return None

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email': ['vinicius@indicium.tech'],
    'email_on_failure': True,
    'email_on_retry': True,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

with DAG(
    'Desafio-3-Lighthouse',
    default_args=default_args,
    description='',
    schedule_interval=timedelta(days=1),
    start_date=datetime(2021, 1, 1),
    catchup=False,
    tags=['example'],
) as dag:
    dag.doc_md = """
        Esse é o desafio de Airflow da Indicium.
    """
   
    order_to_csv = PythonOperator(
        task_id='order_to_csv',
        python_callable=read_db.order_to_csv,
        provide_context=True
    )

    count_quantity_rio = PythonOperator(
        task_id='count_quantity_rio',
        python_callable=read_db.count_quantity_rio,
        provide_context=True
    )

    export_final_output = PythonOperator(
        task_id='export_final_output',
        python_callable=export_final_answer,
        provide_context=True
    )

order_to_csv >> count_quantity_rio >> export_final_output